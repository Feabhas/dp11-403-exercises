#include "DateTime.h"
#include <chrono>
#include <ctime>

using namespace std;

string DateTime::now() {
  auto now = std::time(nullptr);
  return string{std::asctime(std::localtime(&now))};
}