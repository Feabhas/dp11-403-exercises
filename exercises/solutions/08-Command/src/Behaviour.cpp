#include "Behaviour.h"

#include "Hardware.h"

void MotorBehaviour::execute() {
  if (isOn) {
    context.off();
  } else {
    context.on();
  }
  isOn = not isOn;
}
