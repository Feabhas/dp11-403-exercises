#ifndef I_Waypoint_H
#define I_Waypoint_H

class I_Waypoint {
public:
  virtual void display() const = 0;
  virtual ~I_Waypoint() = default;
};

#endif // I_Waypoint_H
