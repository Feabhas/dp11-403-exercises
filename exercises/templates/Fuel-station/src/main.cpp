#include "Console.h"
#include "DuelFuelPump.h"
#include "SingleFuelPump.h"

int main() 
{
  SingleFuelPump petrol{};
  DuelFuelPump petrolDiesel{};

  petrol.buyFuel1(47.2);
  petrolDiesel.buyFuel1(32.5);
  petrolDiesel.buyFuel2(24.1);
}