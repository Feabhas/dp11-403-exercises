#ifndef IRunnable_H
#define IRunnable_H

#include "RunPolicies.h"

class IRunnable {
public:
  virtual bool run() = 0;
  virtual ~IRunnable() {}
};

#endif // IRunnable_H
