#ifndef Sequencer_H
#define Sequencer_H

#include "StateMachine.h"

class Sequencer {
public:
  Sequencer() = default;

  void play();
  void stop();
  void record();

private:
  StateMachine::ptr current_state{StateMachine::initialise()};
};

#endif // Sequencer_H