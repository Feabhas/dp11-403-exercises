#ifndef DATETIME_H
#define DATETIME_H

#include <string>

class DateTime {
public:
  static std::string now();
};

#endif // DATETIME_H